export * from '@formkit/tempo'
export { format as formatDate } from '@formkit/tempo'

const DEFAULT_LOCALE = 'ru-RU'

const CURRENCY_OPTIONS: Intl.NumberFormatOptions = { style: 'currency', currency: 'RUB', maximumFractionDigits: 0 }
const PERCENT_OPTIONS: Intl.NumberFormatOptions = { style: 'percent', useGrouping: false }

/**
 * Форматирует число согласно указанному языку и параметрам.
 * @param n Число для форматирования.
 * @param options Параметры для форматирования.
 * @param locale Язык для форматирования.
 * @returns Отформатированное число.
 */
export function formatNumber(
  n: number | bigint,
  options: Intl.NumberFormatOptions = {},
  locale = DEFAULT_LOCALE,
) {
  return new Intl.NumberFormat(locale, options).format(n)
}

/**
 * Форматирует число как валюту согласно указанным параметрам.
 * @param n Число для форматирования.
 * @param options Параметры для форматирования.
 * @returns Отформатированное число.
 */
export function formatCurrency(
  n: number | bigint,
  options: Intl.NumberFormatOptions = {},
) {
  return formatNumber(n, { ...CURRENCY_OPTIONS, ...options })
}

/**
 * Форматирует число как процент согласно указанным параметрам.
 * @param n Число для форматирования.
 * @param options Параметры для форматирования.
 * @returns Отформатированное число.
 */
export function formatPercent(
  n: number | bigint,
  options: Intl.NumberFormatOptions = {},
) {
  return formatNumber(n, { ...PERCENT_OPTIONS, ...options })
}
