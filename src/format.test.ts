import { describe, expect, it } from 'vitest'
import { formatCurrency, formatNumber, formatPercent } from './format'

describe('formatNumber', () => {
  it('should format numbers correctly', () => {
    expect(formatNumber(10)).toEqual('10')
    expect(formatNumber(100)).toEqual('100')
    expect(formatNumber(1_000)).toEqual('1\xA0000')
  })

  it('should format numbers correctly when pass options', () => {
    expect(formatNumber(10.0000, { maximumFractionDigits: 1 })).toEqual('10')
    expect(formatNumber(1_000, { useGrouping: false })).toEqual('1000')
  })

  it('should format numbers correctly when pass empty options', () => {
    expect(formatNumber(1000, {})).toEqual('1\xA0000')
    expect(formatNumber(1000, undefined)).toEqual('1\xA0000')
  })
})

describe('formatCurrency', () => {
  it('should format currency correctly (RUB)', () => {
    expect(formatCurrency(100)).toEqual('100\xA0₽')
    expect(formatCurrency(1_000)).toEqual('1\xA0000\xA0₽')
  })

  it('should format currency correctly (USD)', () => {
    expect(formatCurrency(100, { currency: 'USD' })).toEqual('100\xA0$')
    expect(formatCurrency(1_000, { currency: 'USD' })).toEqual('1\xA0000\xA0$')
  })
})

describe('formatPercent', () => {
  it('should format percent correctly', () => {
    expect(formatPercent(0.22)).toEqual('22\xA0%')
    expect(formatPercent(0.99)).toEqual('99\xA0%')
    expect(formatPercent(1)).toEqual('100\xA0%')
    expect(formatPercent(10)).toEqual('1000\xA0%')
  })
})
