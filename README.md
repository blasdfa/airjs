# @air/airjs

## Использование

### Установка

```bash
pnpm i -D eslint @air/airjs
```

### Импорт

Импорты сгруппированы по функциональным возможностям.

```js
// Импорт типовв
import { ElementOf, Fn } from '@air/airjs/types'

// Импорт функций форматирования
import {
  formatCurrency,
  formatDate,
  formatNumber,
  formatPercent
} from '@air/airjs/format'
```
