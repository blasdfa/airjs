import { defineBuildConfig } from 'unbuild'

export default defineBuildConfig({
  entries: [
    'src/format',
    'src/types',
  ],
  declaration: true,
  clean: true,
  rollup: {
    emitCJS: true,
  },
  externals: ['@antfu/utils', '@formkit/tempo'],
})
